import './App.css';

import { Header } from './components/Header';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Register } from './components/Register';
import SignIn from './components/LogIn';

function App() {
  
  // useEffect( async () => {

	// 	const taskFromServer = await fetchProfileUser();
	// 	console.log("taskFromServer");
	// 	console.log(taskFromServer);

  //       // const getProfileUser = async () => {
  //       //     const taskFromServer = await fetchProfileUser();
	// 	// 	console.log("taskFromServer");
	// 	// 	console.log(taskFromServer);
  //       // }
  //       // getProfileUser();
		
  //   }, []);

	//   const fetchProfileUser = async () => {
  //       const res = await fetch(`http://localhost:8080/profile`);
  //       const data = await res.json();
  //       return data;
  //   }
  return (
    <div className="App">
      <Router>
        <Route exact path='/' component={Header} />
        <Route path='/register' component={Register} />
        <Route path='/sign-in' component={SignIn} />
        
      </Router>
    </div>
  );
}

export default App;
