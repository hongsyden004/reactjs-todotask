import React from 'react'
import { createContext, useReducer, useEffect } from 'react';
import Reducer from './Reducer';
import propTypes from 'prop-types'

var userData = null;
if(localStorage.getItem("user") !== 'null') {
    userData = JSON.stringify(localStorage.getItem("user"))
}

const INITIAL_STATE = {
    user: userData,
    isFetching: false,
    error: false,
    dispatch: propTypes.func
};

export const Context = createContext(INITIAL_STATE);

export const ContextProvider = ({children}) => {
    const [state, dispatch] = useReducer(Reducer, INITIAL_STATE)
    useEffect(() => {
        
        localStorage.setItem("user", JSON.stringify(state.user))

    }, [state.user])

    return (
        <Context.Provider value={{
            user: state.user,
            isFetching: state.isFetching,
            error: state.error,
            dispatch,
        }}>
            {children}
        </Context.Provider>
    )
}
