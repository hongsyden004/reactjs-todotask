import { useState, useContext } from "react";
import { useHistory } from 'react-router-dom'
import { Context } from "./Context";
import axios from 'axios';

const SignIn = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const history = useHistory();

    // const fetchUserByPhonePwd = async (phone, pwd) => {
    //     const res = await fetch(`http://localhost:8080/users?phone=${phone}&password=${pwd}`);
    //     const data = await res.json();
    //     return data;
    // }

    const userLoginbyEmail = async (datas) => {
        const res = await axios.post('https://api-nodejs-todolist.herokuapp.com/user/login', datas)
                        .then( ( {data} ) =>  {
                            console.log(data)
                            return data;
                        } )
                        .catch(function (error) {
                            // handle error
                            console.log(error);
                        })
        return res;
    }

    const { dispatch, isFetching } = useContext(Context);

    const onSubmitTask = async (e) => {
        e.preventDefault();

        dispatch({type: "LOGIN_START"});

        try {
            // var userLogin = await fetchUserByPhonePwd(email, password);
            var userLogin = await userLoginbyEmail({ email, password })
            
            if(userLogin && userLogin.token) {
                dispatch({type: "LOGIN_SUCESS", payload: userLogin});
                history.push('/');
            } else {
                dispatch({type: "LOGIN_FAILURE"});
            }
        } catch (error) {
            dispatch({type: "LOGIN_FAILURE"});
        }
    }

    return(
        <>
            <div className="container">
                <br />
                <h3>Please login the from below</h3>
                <form onSubmit={onSubmitTask}>
                    
                    <div className="form-group">
                        <label htmlFor="email">Email:</label>
                        <input type="text" className="form-control" id="email" placeholder="Enter email" name="email" value={email} onChange={ (e) => setEmail(e.target.value) } />
                    </div>
                    <div className="form-group">
                        <label htmlFor="pwd">Password:</label>
                        <input type="password" className="form-control" id="pwd" placeholder="Enter password" name="pwd" value={password} onChange={ (e) => setPassword(e.target.value) } />
                    </div>

                    <button type="submit" className="btn btn-primary btn-login" disabled={isFetching}>Submit</button>
                </form>
            </div>
        </>
    )
}

export default SignIn
