import React, { useState } from 'react'
import { useHistory } from 'react-router-dom'
import axios from 'axios';

export const Register = () => {
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [age, setAge] = useState('')
    const history = useHistory()

    const onSubmitTask = async (e) => {
        e.preventDefault();
        if(!name) {
            alert('Please enter your name!');
            return;
        }
        if(!age){
            alert('Please enter your age');
            return;
        }
        if(!email){
            alert('Please enter your gmail address');
            return;
        }
        if(!password){
            alert('Please enter your pasword');
            return;
        }
        var resultAddUser = await addNewUser( {name, age, email, password} );
        setName('')
        setAge('')
        setEmail('')
        setPassword('')
        if(resultAddUser){
            history.push('/');
        }
    }

    const addNewUser = async (datas) => {
        const res = await axios.post('https://api-nodejs-todolist.herokuapp.com/user/register', datas)
                        .then( ( {data} ) =>  {
                            console.log(data)
                            return data;
                        } )
                        .catch(function (error) {
                            // handle error
                            console.log(error);
                        })
        return res;
    }

    return (
        <div className='register'>
            <div className="container">
                <br />
                <h3>Please register the from below</h3>
                <form onSubmit={onSubmitTask}>
                    <div className="form-group">
                        <label htmlFor="name">Name:</label>
                        <input type="text" className="form-control" id="name" placeholder="Enter name" name="name" value={name} onChange={ (e) => setName(e.target.value) } />
                    </div>
                    <div className="form-group">
                        <label htmlFor="email">Email:</label>
                        <input type="email" className="form-control" id="email" placeholder="Enter email" name="email" value={email} onChange={ (e) => setEmail(e.target.value) } />
                    </div>
                    <div className="form-group">
                        <label htmlFor="pwd">Password:</label>
                        <input type="password" className="form-control" id="pwd" placeholder="Enter password" name="pwd" value={password} onChange={ (e) => setPassword(e.target.value) } />
                    </div>

                    <div className="form-group">
                        <label htmlFor="age">Age:</label>
                        <input type="text" className="form-control" id="age" placeholder="Enter age" name="age" value={age} onChange={ (e) => setAge(e.target.value) } />
                    </div>

                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    )
}
